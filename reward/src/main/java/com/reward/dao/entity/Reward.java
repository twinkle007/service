package com.reward.dao.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Reward {
int reward_id;
int title;
String description;
int status;
Date start_date;
Date end_date;
int type;
int total_chop;
int inventory;
int category_id;
int merchant_id;


@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
@JoinColumn(name = "reward_id")
private Wallet wallet;


@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
@JoinColumn(name = "reward_id")
private Transaction transaction;





public int getReward_id() {
	return reward_id;
}
public void setReward_id(int reward_id) {
	this.reward_id = reward_id;
}
public int getTitle() {
	return title;
}
public void setTitle(int title) {
	this.title = title;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public int getStatus() {
	return status;
}
public void setStatus(int status) {
	this.status = status;
}
public Date getStart_date() {
	return start_date;
}
public void setStart_date(Date start_date) {
	this.start_date = start_date;
}
public Date getEnd_date() {
	return end_date;
}
public void setEnd_date(Date end_date) {
	this.end_date = end_date;
}
public int getType() {
	return type;
}
public void setType(int type) {
	this.type = type;
}
public int getTotal_chop() {
	return total_chop;
}
public void setTotal_chop(int total_chop) {
	this.total_chop = total_chop;
}
public int getInventory() {
	return inventory;
}
public void setInventory(int inventory) {
	this.inventory = inventory;
}
public int getCategory_id() {
	return category_id;
}
public void setCategory_id(int category_id) {
	this.category_id = category_id;
}
public int getMerchant_id() {
	return merchant_id;
}
public void setMerchant_id(int merchant_id) {
	this.merchant_id = merchant_id;
}

}
