package com.reward.dao.entity;

import java.util.Date;

import javax.persistence.Entity;

@Entity
public class Transaction {
	
	int transaction_id;
	Date transaction_date;
	int from_state;
	int to_state;
	int reward_type;
	int reward_id;
	int user_id;
	
	public int getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(int transaction_id) {
		this.transaction_id = transaction_id;
	}
	public Date getTransaction_date() {
		return transaction_date;
	}
	public void setTransaction_date(Date transaction_date) {
		this.transaction_date = transaction_date;
	}
	public int getFrom_state() {
		return from_state;
	}
	public void setFrom_state(int from_state) {
		this.from_state = from_state;
	}
	public int getTo_state() {
		return to_state;
	}
	public void setTo_state(int to_state) {
		this.to_state = to_state;
	}
	public int getReward_type() {
		return reward_type;
	}
	public void setReward_type(int reward_type) {
		this.reward_type = reward_type;
	}
	public int getReward_id() {
		return reward_id;
	}
	public void setReward_id(int reward_id) {
		this.reward_id = reward_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	
}
